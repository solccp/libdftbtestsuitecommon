#include "moleculegeometry.h"

#include <QTextStream>
#include <QStringList>
#include <QString>
#include <QMap>
#include <QSet>

#include "atomicproperties.h"

#include <array>
#include <cmath>

namespace DFTBTestSuite
{

MoleculeGeometry::MoleculeGeometry()
{
}

bool MoleculeGeometry::readXYZString(const QString &geometry)
{
    clear();
    QStringList lines = geometry.split("\n", QString::KeepEmptyParts);
    int nat = lines.at(0).toInt();
    Q_ASSERT_X(lines.size()-2 == nat, "MoleculeGeometry::readString", "wrong xyz format");

    for(int i=2; i<lines.size();++i)
    {
        QStringList arr = lines.at(i).split(" ", QString::SkipEmptyParts);

        const ADPT::AtomicProperties *atom = ADPT::AtomicProperties::fromSymbol(arr[0]);
        if (atom == &ADPT::AtomicProperties::ERROR_ATOM)
        {
            clear();
            return false;
        }
        bool ok;
        atom_symbols.append(arr[0]);
        std::array<double, 3> coord;
        coord[0] = arr.at(1).toDouble(&ok);
        if (!ok)
        {
            clear();
            return false;
        }
        coord[1] = arr.at(2).toDouble(&ok);
        if (!ok)
        {
            clear();
            return false;
        }
        coord[2] = arr.at(3).toDouble(&ok);
        if (!ok)
        {
            clear();
            return false;
        }
        coords.append(coord);
    }
    return true;
}

int MoleculeGeometry::NumAtom() const
{
    return coords.size();
}

double MoleculeGeometry::getDistance(int index1, int index2) const
{
    double res = 0.0 ;
    const std::array<double, 3> &coord1 = coords[index1-1];
    const std::array<double, 3> &coord2 = coords[index2-1];
    res = vectorLength(vectorSubstract(coord1, coord2));
    return res;
}

double MoleculeGeometry::getAngle(int index1, int middle, int index3) const
{
    const std::array<double, 3> &p1 = coords[index1-1];
    const std::array<double, 3> &p2 = coords[middle-1];
    const std::array<double, 3> &p3 = coords[index3-1];

    std::array<double, 3> v1 = vectorSubstract(p1,p2);
    std::array<double, 3> v2 = vectorSubstract(p3,p2);

    if (vectorLength(v1) < 1.0e-3 || vectorLength(v2) < 1.0e-3)
    {
        return 0.0;
    }

    return getAngle_impl(v1, v2);
}

double MoleculeGeometry::getTorsion(int index1, int index2, int index3, int index4) const
{
    const std::array<double, 3> &p1 = coords[index1-1];
    const std::array<double, 3> &p2 = coords[index2-1];
    const std::array<double, 3> &p3 = coords[index3-1];
    const std::array<double, 3> &p4 = coords[index4-1];

    std::array<double, 3> b1 = vectorSubstract(p1,p2);
    std::array<double, 3> b2 = vectorSubstract(p2,p3);
    std::array<double, 3> b3 = vectorSubstract(p3,p4);

    double torsion;

    std::array<double, 3> c1,c2,c3;
    c1 = vectorCross(b1,b2);
    c2 = vectorCross(b2,b3);
    c3 = vectorCross(c1,c2);

    if ( vectorLength(c1)*vectorLength(c2) < 0.001)
    {
      torsion = 0.0;
      return torsion;
    }

    torsion = getAngle_impl(c1,c2);
    if (vectorDot(b2,c3) > 0.0)
      torsion = -torsion;

    if (!std::isfinite(torsion))
      torsion = 180.0;

    return(torsion);

}

void MoleculeGeometry::setCoord(int atomIndex, int coordIndex, double value)
{
    this->coords[atomIndex][coordIndex] = value;
}

const QList<QString> &MoleculeGeometry::getElements() const
{
    return atom_symbols;
}

QString MoleculeGeometry::getAtomSymbol(int index) const
{
    return atom_symbols[index];
}

std::array<double, 3> MoleculeGeometry::getCoord(int index) const
{
    return coords[index];
}

bool MoleculeGeometry::isLinear() const
{
    if (this->NumAtom() <= 2)
    {
        return true;
    }
    else
    {
        for(int i = 2; i<this->NumAtom(); ++i)
        {
            double angle = this->getAngle(1,2,i+1);
            if ( angle < 179.999 || angle > 0.001 )
            {
                return false;
            }
        }
    }
    return true;
}

void MoleculeGeometry::clear()
{
    this->coords.clear();
    this->atom_symbols.clear();
}

void MoleculeGeometry::addAtom(const QString &element, double x, double y, double z)
{
    const ADPT::AtomicProperties* atom = ADPT::AtomicProperties::fromSymbol(element);

    if (atom == &ADPT::AtomicProperties::ERROR_ATOM)
    {
        return;
    }
    atom_symbols.push_back(atom->getSymbol());
    std::array<double, 3> coord;
    coord[0] = x;
    coord[1] = y;
    coord[2] = z;
    coords.push_back(coord);
}

QString MoleculeGeometry::getFormula() const
{
    QMap<QString, int> count;
    for(int i=0; i<atom_symbols.size();++i)
    {
        if (!count.contains(atom_symbols[i]))
        {
            count[atom_symbols[i]] = 1;
        }
        else
        {
            count[atom_symbols[i]]++;
        }
    }
    QMapIterator<QString, int> it(count);
    QString res;
    while(it.hasNext())
    {
        it.next();
        res += QString("%1%2").arg(it.key()).arg(it.value());
    }
    return res;
}

QString MoleculeGeometry::toXYZ() const
{
    QByteArray array;
    QTextStream sout(&array);

    sout << atom_symbols.size() << endl;
    sout << endl;
    for(int i=0; i<this->atom_symbols.size();++i)
    {
        sout << QString("%1 %2 %3 %4").arg(atom_symbols[i],2)
                .arg(coords[i][0],20,'f',12)
                .arg(coords[i][1],20,'f',12)
                .arg(coords[i][2],20,'f',12)
                << endl;
    }
    sout.flush();
    return QString(array);
}

QString MoleculeGeometry::toNoHeaderXYZ() const
{
    QByteArray array;
    QTextStream sout(&array);
    for(int i=0; i<this->atom_symbols.size();++i)
    {
        sout << QString("%1 %2 %3 %4").arg(atom_symbols[i],2)
                .arg(coords[i][0],20,'f',12)
                .arg(coords[i][1],20,'f',12)
                .arg(coords[i][2],20,'f',12)
                << endl;
    }
    sout.flush();
    return QString(array);
}

QString MoleculeGeometry::toGen() const
{
    QByteArray array;
    QTextStream sout(&array);
    QSet<QString> elements = this->atom_symbols.toSet();
    QList<QString> elementslist = elements.toList();
    qSort(elementslist.begin(), elementslist.end());
    sout << atom_symbols.size() << " C" << endl;
    sout << endl;
    for(int i=0; i<this->atom_symbols.size();++i)
    {
        sout << QString("%1 %2 %3 %4 %5")
                .arg(i+1)
                .arg(elementslist.indexOf(atom_symbols[i])+1,3)
                .arg(coords[i][0],20,'f',12)
                .arg(coords[i][1],20,'f',12)
                .arg(coords[i][2],20,'f',12)
                << endl;
    }
    sout.flush();
    return QString(array);
}

double MoleculeGeometry::getAngle_impl(const std::array<double, 3> &v1, const std::array<double, 3> &v2) const
{
    const double RAD_TO_DEG = 180.0/3.14159265;
    double dp;
    dp = vectorDot(v1,v2)/(vectorLength(v1)*vectorLength(v2));
    if (dp > 1.0)
    {
        return 0.0;
    }
    else if (dp < -1.0)
    {
        return 180.0;
    }
    return((RAD_TO_DEG * acos(dp)));
}

std::array<double, 3> MoleculeGeometry::vectorSubstract(const std::array<double, 3> &v1, const std::array<double, 3> &v2) const
{
    std::array<double, 3> diff;
    std::transform(v1.begin(), v1.end(), v2.begin(), diff.begin(), std::minus<double>());
    return diff;
}

std::array<double, 3> MoleculeGeometry::vectorCross(const std::array<double, 3> &v1, const std::array<double, 3> &v2) const
{
    std::array<double, 3> vv;

    vv[0] =   v1[1]*v2[2] - v1[2]*v2[1] ;
    vv[1] = - v1[0]*v2[2] + v1[2]*v2[0] ;
    vv[2] =   v1[0]*v2[1] - v1[1]*v2[0] ;

    return ( vv ) ;
}

double MoleculeGeometry::vectorLength(const std::array<double, 3> &v) const
{
    double res;
    res = std::sqrt(vectorDot(v,v));
    return res;
}

double MoleculeGeometry::vectorDot(const std::array<double, 3> &v1, const std::array<double, 3> &v2) const
{
    return (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2]);
}

}
