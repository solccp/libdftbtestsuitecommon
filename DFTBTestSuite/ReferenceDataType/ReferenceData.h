#ifndef REFERENCEDATA_H
#define REFERENCEDATA_H

#include "MolecularData.h"
#include "CrystalData.h"
#include "ReactionData.h"
#include "AtomicData.h"


namespace DFTBTestSuite
{

struct ReferenceData
{
    QDateTime createdDateTime = QDateTime::currentDateTime();
    CrystalData crystalData;
    QList<MolecularData>  molecularDataSet;

    QList<ReferenceReactionData> reactionDataSet;
    QList<AtomicData> atomicDataSet;

    QMap<QString, int> atomicDataIndexMapping;
    QMap<QString, int>  molecularDataIndexMapping;
};

}

#endif // REFERENCEDATA_H
