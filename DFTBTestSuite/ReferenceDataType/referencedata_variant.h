#ifndef REFERENCEDATA_VARIANT
#define REFERENCEDATA_VARIANT

#include <Variant/Variant.h>
#include "variantqt.h"

#include "ReferenceData.h"
#include "atomicproperties.h"

namespace libvariant
{

    template<>
    struct VariantConvertor<DFTBTestSuite::AtomicData>
    {
        static Variant toVariant(const DFTBTestSuite::AtomicData &rhs)
        {
            Variant var;
            var["energy"] = rhs.energy;
            var["unit"] = rhs.energy_unit.toStdString();
            var["element"] = rhs.symbol.toStdString();
            var["method"] = rhs.method.toStdString();
            var["basis_set"] = rhs.basisSet.toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::AtomicData &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.method, var, "method", QString, hasValue);
            GET_VARIANT_OPT(rhs.basisSet, var, "basis_set", QString, hasValue);

            GET_VARIANT(rhs.energy, var, "energy", double);
            GET_VARIANT(rhs.energy_unit, var, "unit", QString);
            GET_VARIANT(rhs.symbol, var, "element", QString);

            Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::AtomCoordinate>
    {
        static Variant toVariant(const DFTBTestSuite::AtomCoordinate &rhs)
        {
            Variant var;
            var.Append(rhs.symbol.toStdString());
            var.Append(rhs.position[0]);
            var.Append(rhs.position[1]);
            var.Append(rhs.position[2]);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::AtomCoordinate &rhs)
        {
            GET_VARIANT_LIST(rhs.symbol, var, 0, QString);
            GET_VARIANT_LIST(rhs.position[0], var, 1, double);
            GET_VARIANT_LIST(rhs.position[1], var, 2, double);
            GET_VARIANT_LIST(rhs.position[2], var, 3, double);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::MoleculeGeometry>
    {
        static Variant toVariant(const DFTBTestSuite::MoleculeGeometry &rhs)
        {
            Variant var;
            for (int i=0; i < rhs.NumAtom(); i++)
            {
                Variant line;
                line.Append(rhs.getAtomSymbol(i).toStdString());
                auto const &coord = rhs.getCoord(i);
                line.Append(coord[0]);
                line.Append(coord[1]);
                line.Append(coord[2]);
                var.Append(line);
            }
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::MoleculeGeometry &rhs)
        {
            QList<DFTBTestSuite::AtomCoordinate> coords;
            for(auto i=0u; i<var.Size(); ++i)
            {
                DFTBTestSuite::AtomCoordinate coord;
                GET_VARIANT_LIST(coord, var, i, DFTBTestSuite::AtomCoordinate);
                coords.append(coord);
            }
            rhs.clear();
            foreach(auto const & coor, coords)
            {
                rhs.addAtom(coor.symbol, coor.position[0], coor.position[1], coor.position[2]);
            }
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::GeometryData>
    {
        static Variant toVariant(const DFTBTestSuite::GeometryData &rhs)
        {
            Variant var;
            var["method"] = rhs.method.toStdString();
            var["basis_set"] = rhs.basisSet.toStdString();
            var["stationary"] = rhs.optimized;


            var["coordinates"] = VariantConvertor<DFTBTestSuite::MoleculeGeometry>::toVariant(rhs.geometry);

            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::GeometryData &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.method, var, "method", QString, hasValue);
            GET_VARIANT_OPT(rhs.basisSet, var, "basis_set", QString, hasValue);

            GET_VARIANT(rhs.optimized, var, "stationary", bool);

            GET_VARIANT(rhs.geometry, var, "coordinates", DFTBTestSuite::MoleculeGeometry);

            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::TotalEnergyData>
    {
        static Variant toVariant(const DFTBTestSuite::TotalEnergyData &rhs)
        {
            Variant var;
            var["method"] = rhs.method.toStdString();
            var["basis_set"] = rhs.basisSet.toStdString();
            var["energy"] = rhs.energy;
            var["unit"] = rhs.energy_unit.toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::TotalEnergyData &rhs)
        {
            GET_VARIANT(rhs.energy, var, "energy", double);
            GET_VARIANT(rhs.energy_unit, var, "unit", QString);

            bool hasValue;
            GET_VARIANT_OPT(rhs.method, var, "method", QString, hasValue);
            GET_VARIANT_OPT(rhs.basisSet, var, "basis_set", QString, hasValue);
            rhs.isValid = true;
            Q_UNUSED(hasValue);
        }
    };
    template<>
    struct VariantConvertor<DFTBTestSuite::AtomizationEnergyData>
    {
        static Variant toVariant(const DFTBTestSuite::AtomizationEnergyData &rhs)
        {
            Variant var;
            var["method"] = rhs.method.toStdString();
            var["basis_set"] = rhs.basisSet.toStdString();
            var["energy"] = rhs.energy;
            var["unit"] = rhs.energy_unit.toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::AtomizationEnergyData &rhs)
        {
            GET_VARIANT(rhs.energy, var, "energy", double);
            GET_VARIANT(rhs.energy_unit, var, "unit", QString);

            bool hasValue;
            GET_VARIANT_OPT(rhs.method, var, "method", QString, hasValue);
            GET_VARIANT_OPT(rhs.basisSet, var, "basis_set", QString, hasValue);
            rhs.isValid = true;
            Q_UNUSED(hasValue);
        }
    };
    template<>
    struct VariantConvertor<DFTBTestSuite::FrequencyData>
    {
        static Variant toVariant(const DFTBTestSuite::FrequencyData &rhs)
        {
            Variant var;
            var["method"] = rhs.method.toStdString();
            var["basis_set"] = rhs.basisSet.toStdString();
            var["frequencies"] = VariantConvertor<QList<double> >::toVariant(rhs.frequencies);
            if (rhs.weights.size() == rhs.frequencies.size())
            {
                var["weights"] = VariantConvertor<QList<double> >::toVariant(rhs.weights);
            }
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::FrequencyData &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.method, var, "method", QString, hasValue);
            GET_VARIANT_OPT(rhs.basisSet, var, "basis_set", QString, hasValue);

            GET_VARIANT(rhs.frequencies, var, "frequencies", QList<double>);
            GET_VARIANT_OPT(rhs.weights, var, "weights", QList<double>, hasValue);
            if (hasValue)
            {
                if (rhs.weights.size() != rhs.frequencies.size())
                {
                    rhs.weights.clear();
                }
            }
            rhs.isValid = true;
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::MolecularData>
    {
        static Variant toVariant(const DFTBTestSuite::MolecularData &rhs)
        {
            Variant var;
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            var["name"] = rhs.name.toStdString();
            var["charge"] = rhs.charge;
            var["spin"] = rhs.spin;
            var["geometry"] = VariantConvertor<DFTBTestSuite::GeometryData>::toVariant(rhs.geometryData);
            if (rhs.totalEnergyData.isValid)
            {
                var["total_energy"] = VariantConvertor<DFTBTestSuite::TotalEnergyData>::toVariant(rhs.totalEnergyData);
            }
            if (rhs.atomizationEnergyData.isValid)
            {
                var["atomization_energy"] = VariantConvertor<DFTBTestSuite::AtomizationEnergyData>::toVariant(rhs.atomizationEnergyData);
            }
            if (rhs.frequencyData.isValid)
            {
                var["frequency_data"] = VariantConvertor<DFTBTestSuite::FrequencyData>::toVariant(rhs.frequencyData);
            }
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::MolecularData &rhs)
        {
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);

            GET_VARIANT(rhs.geometryData, var, "geometry", DFTBTestSuite::GeometryData);

            bool hasValue;
            GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);
            GET_VARIANT_OPT(rhs.charge, var, "charge", int, hasValue);
            GET_VARIANT_OPT(rhs.spin, var, "spin", int, hasValue);
            GET_VARIANT_OPT(rhs.totalEnergyData, var, "total_energy", DFTBTestSuite::TotalEnergyData, hasValue);
            GET_VARIANT_OPT(rhs.atomizationEnergyData, var, "atomization_energy", DFTBTestSuite::AtomizationEnergyData, hasValue);
            GET_VARIANT_OPT(rhs.frequencyData, var, "frequency_data", DFTBTestSuite::FrequencyData, hasValue);
            Q_UNUSED(hasValue);
        }
    };




    template<>
    struct VariantConvertor<DFTBTestSuite::BandStructureTestEntry>
    {
        static Variant toVariant(const DFTBTestSuite::BandStructureTestEntry &rhs)
        {
            Variant var;
            if (rhs.type.toLower() == "index_based")
            {
                var["band_end"] = rhs.bandIndex_end;
                var["band_start"] = rhs.bandIndex_start;
                var["point_end"] = rhs.pointIndex_end;
                var["point_start"] = rhs.pointIndex_start;
                var["type"] = "index_based";
            }
            else if (rhs.type.toLower() == "energy_based")
            {
                var["lower_bound"] = rhs.lower_energy;
                var["upper_bound"] = rhs.upper_energy;
                var["type"] = "energy_based";
            }
            var["weight"] = rhs.weight;

            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::BandStructureTestEntry &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.weight, var, "weight", double, hasValue);
            GET_VARIANT(rhs.type, var, "type", QString);

            if (rhs.type.toLower() == "index_based")
            {
                GET_VARIANT(rhs.bandIndex_end, var, "band_end", int);
                GET_VARIANT(rhs.bandIndex_start, var, "band_start", int);
                GET_VARIANT(rhs.pointIndex_end, var, "point_end", int);
                GET_VARIANT(rhs.pointIndex_start, var, "point_start", int);
            }
            else if (rhs.type.toLower() == "energy_based")
            {
                GET_VARIANT(rhs.lower_energy, var, "lower_bound", double);
                GET_VARIANT(rhs.upper_energy, var, "upper_bound", double);
            }
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::KLineEntry>
    {
        static Variant toVariant(const DFTBTestSuite::KLineEntry &rhs)
        {
            Variant var;
            var.Append(rhs.nPoints);
            var.Append(rhs.position[0]);
            var.Append(rhs.position[1]);
            var.Append(rhs.position[2]);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::KLineEntry &rhs)
        {
            GET_VARIANT_LIST(rhs.nPoints, var, 0, int);
            GET_VARIANT_LIST(rhs.position[0], var, 1, double);
            GET_VARIANT_LIST(rhs.position[1], var, 2, double);
            GET_VARIANT_LIST(rhs.position[2], var, 3, double);
        }
    };



    template<>
    struct VariantConvertor<DFTBTestSuite::CrystalGeometryData>
    {
        static Variant toVariant(const DFTBTestSuite::CrystalGeometryData &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["method"] = rhs.method.toStdString();;
            var["basis_set"] = rhs.basis.toStdString();
            var["charge"] = rhs.charge;
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);

            if (rhs.kpoints.numOfPoints() > 0)
            {
                var["kpoints"] = VariantConvertor<ADPT::KPointsSetting>::toVariant(rhs.kpoints);
            }
            var["lattice_vectors"] = VariantConvertor<dMatrix3x3>::toVariant(rhs.lattice_vectors);
            var["scaling_factor"] =  rhs.scaling_factor;
            var["fractional_coordinates"] = rhs.fractional_coordinates;
            var["coordinates"] = VariantConvertor<QList<DFTBTestSuite::AtomCoordinate> >::toVariant(rhs.coordinates);
            if (rhs.spinpol)
            {
                var["spinpol"] = true;
                var["upe"] = rhs.upe;
                var["opt_upe"] = rhs.opt_upe;
            }
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::CrystalGeometryData &rhs)
        {
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);

            GET_VARIANT(rhs.kpoints, var, "kpoints", ADPT::KPointsSetting);
            GET_VARIANT(rhs.lattice_vectors, var, "lattice_vectors", dMatrix3x3);

            GET_VARIANT(rhs.fractional_coordinates, var, "fractional_coordinates", bool);
            GET_VARIANT(rhs.coordinates, var, "coordinates", QList<DFTBTestSuite::AtomCoordinate>);

            bool hasValue;
            GET_VARIANT_OPT(rhs.scaling_factor, var, "scaling_factor", double, hasValue);
            GET_VARIANT_OPT(rhs.basis, var, "basis_set", QString, hasValue);
            GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);
            GET_VARIANT_OPT(rhs.method, var, "method", QString, hasValue);
            GET_VARIANT_OPT(rhs.charge, var, "charge", int, hasValue);

            GET_VARIANT_OPT(rhs.spinpol, var, "spinpol", bool, hasValue);
            if (rhs.spinpol)
            {
                GET_VARIANT(rhs.upe, var, "upe", double);
                GET_VARIANT_OPT(rhs.opt_upe, var, "opt_upe", bool, hasValue);
            }

            Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::BandStructureData>
    {
        static Variant toVariant(const DFTBTestSuite::BandStructureData &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["method"] = rhs.method.toStdString();;
            var["basis_set"] = rhs.basis.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            var["geom_uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.geom_uuid);

            var["bands"] = VariantConvertor<QVector<QVector<double> > >::toVariant(rhs.bands);
            if (!rhs.bands_beta.isEmpty())
            {
                var["bands_beta"] = VariantConvertor<QVector<QVector<double> > >::toVariant(rhs.bands_beta);
            }

            var["default_optimization_target"] = VariantConvertor<QList<DFTBTestSuite::BandStructureTestEntry> >::toVariant(rhs.defaultTestRegion);

            if (rhs.fermiIndex.first != -1 || rhs.fermiIndex.second != -1)
            {
                var["fermi_index"].Append(rhs.fermiIndex.first);
                var["fermi_index"].Append(rhs.fermiIndex.second);
            }

            if (rhs.kpoints.numOfPoints() > 0)
            {
                var["kpoints"] = VariantConvertor<ADPT::KPointsSetting>::toVariant(rhs.kpoints);
            }

            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::BandStructureData &rhs)
        {
            bool hasValue;
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
            GET_VARIANT(rhs.geom_uuid, var, "geom_uuid", ADPT::UUID);

            GET_VARIANT(rhs.bands, var, "bands", QVector<QVector<double>>);

            GET_VARIANT_OPT(rhs.bands_beta, var, "bands_beta", QVector<QVector<double>>, hasValue);


            GET_VARIANT(rhs.kpoints, var, "kpoints", ADPT::KPointsSetting);


            GET_VARIANT_OPT(rhs.defaultTestRegion, var, "default_optimization_target", QList<DFTBTestSuite::BandStructureTestEntry>, hasValue);
            using pii = QPair<int,int>;
            GET_VARIANT_OPT(rhs.fermiIndex, var, "fermi_index", pii, hasValue);
            GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);
            GET_VARIANT_OPT(rhs.method, var, "method", QString, hasValue);
            GET_VARIANT_OPT(rhs.basis, var, "basis_set", QString, hasValue);
            Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::CrystalData>
    {
        static Variant toVariant(const DFTBTestSuite::CrystalData &rhs)
        {
            Variant var;
            var["crystal"] = VariantConvertor<QList<DFTBTestSuite::CrystalGeometryData> >::toVariant(rhs.geometryData);
            var["bandstructure"] = VariantConvertor<QList<DFTBTestSuite::BandStructureData> >::toVariant(rhs.bandstructureData);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::CrystalData &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.geometryData, var, "crystal", QList<DFTBTestSuite::CrystalGeometryData>, hasValue );
            GET_VARIANT_OPT(rhs.bandstructureData, var, "bandstructure", QList<DFTBTestSuite::BandStructureData>, hasValue );
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::ReactionItem>
    {
        static Variant toVariant(const DFTBTestSuite::ReactionItem &rhs)
        {
            Variant var;
            var["coefficient"] = rhs.coefficient;
            var["geom_uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.geom_uuid);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::ReactionItem &rhs)
        {
            GET_VARIANT(rhs.coefficient, var, "coefficient", double);
            GET_VARIANT(rhs.geom_uuid, var, "geom_uuid", ADPT::UUID);
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::ReferenceReactionData>
    {
        static Variant toVariant(const DFTBTestSuite::ReferenceReactionData &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["method"] = rhs.method.toStdString();;
            var["basis_set"] = rhs.basis.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            var["energy"] = rhs.energy;
            var["unit"] = rhs.energy_unit.toStdString();
            var["reactants"] = VariantConvertor<QList<DFTBTestSuite::ReactionItem> >::toVariant(rhs.reactants);
            var["products"] = VariantConvertor<QList<DFTBTestSuite::ReactionItem> >::toVariant(rhs.products);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::ReferenceReactionData &rhs)
        {

            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
            GET_VARIANT(rhs.energy, var, "energy", double);
            GET_VARIANT(rhs.energy_unit, var, "unit", QString);
            GET_VARIANT(rhs.reactants, var, "reactants", QList<DFTBTestSuite::ReactionItem>);
            GET_VARIANT(rhs.products, var, "products", QList<DFTBTestSuite::ReactionItem>);

            bool hasValue;
            GET_VARIANT_OPT(rhs.name, var, "name", QString, hasValue);
            GET_VARIANT_OPT(rhs.method, var, "method", QString, hasValue);
            GET_VARIANT_OPT(rhs.basis, var, "basis_set", QString, hasValue);
            Q_UNUSED(hasValue);
        }
    };



    template<>
    struct VariantConvertor<DFTBTestSuite::ReferenceData>
    {
        static Variant toVariant(const DFTBTestSuite::ReferenceData &rhs)
        {
            libvariant::Variant var;
            var["timestamp"] = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss.zzz").toStdString();
            {
                var["atomic_data"] = VariantConvertor<QList<DFTBTestSuite::AtomicData> >::toVariant(rhs.atomicDataSet);
            }
            {
                var["molecular_data"] = VariantConvertor<QList<DFTBTestSuite::MolecularData> >::toVariant(rhs.molecularDataSet);
            }
            {
                var["crystal_data"] = VariantConvertor<DFTBTestSuite::CrystalData>::toVariant(rhs.crystalData);
            }
            var["reaction_data"] = VariantConvertor<QList<DFTBTestSuite::ReferenceReactionData> >::toVariant(rhs.reactionDataSet);
            return var;
        }

        static void fromVariant(const libvariant::Variant &var, DFTBTestSuite::ReferenceData &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.atomicDataSet, var, "atomic_data", QList<DFTBTestSuite::AtomicData>, hasValue);
            GET_VARIANT_OPT(rhs.molecularDataSet, var, "molecular_data", QList<DFTBTestSuite::MolecularData>, hasValue);
            GET_VARIANT_OPT(rhs.crystalData, var, "crystal_data", DFTBTestSuite::CrystalData, hasValue);
            GET_VARIANT_OPT(rhs.reactionDataSet, var, "reaction_data", QList<DFTBTestSuite::ReferenceReactionData>, hasValue);

            for (int i = 0; i < rhs.atomicDataSet.size(); ++i)
            {
                const DFTBTestSuite::AtomicData &data = rhs.atomicDataSet[i];
                const ADPT::AtomicProperties *atom = ADPT::AtomicProperties::fromSymbol(data.symbol);
                if (atom != &ADPT::AtomicProperties::ERROR_ATOM)
                {
                    rhs.atomicDataIndexMapping.insert(atom->getSymbol(), i);
                }
            }

            for (int i = 0; i < rhs.molecularDataSet.size(); ++i)
            {
                const DFTBTestSuite::MolecularData &data = rhs.molecularDataSet[i];
                rhs.molecularDataIndexMapping.insert(data.uuid.toString(), i);
            }
            Q_UNUSED(hasValue);
        }
    };
}



#endif // REFERENCEDATA_VARIANT

