#ifndef ATOMICDATA_H
#define ATOMICDATA_H

#include <QString>

namespace DFTBTestSuite
{

struct AtomicData
{
    double energy = 0.0;
    QString energy_unit = "H";
    QString symbol;
    QString method = "";
    QString basisSet = "";
};

}

#endif // ATOMICDATA_H
