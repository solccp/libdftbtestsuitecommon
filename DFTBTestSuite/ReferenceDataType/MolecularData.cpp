#include "MolecularData.h"
#include "AtomicData.h"
#include "atomicproperties.h"
#include <QMap>
#include <QSet>
#include <QDebug>

namespace DFTBTestSuite
{

void calcGeometryProperties(const MoleculeGeometry &ref, GeometryProperty& property)
{
    QMap<int, QList<int>> nebor;
    property.bondAngles.clear();
    property.bondLengths.clear();
    property.nonCovalentBondLengths.clear();
    property.torsionAngles.clear();

    for(int i=0; i<ref.NumAtom(); ++i)
    {
        QString elem1 = ref.getElements().at(i);
        double Rcov1 = ADPT::AtomicProperties::fromSymbol(elem1)->getCovalentRadius();
        for(int j=0; j<i; ++j)
        {
            QString elem2 = ref.getElements().at(j);
            double Rcov2 = ADPT::AtomicProperties::fromSymbol(elem2)->getCovalentRadius();
            double dis = ref.getDistance(i+1,j+1);

            double toler = (Rcov1+Rcov2)+0.4;
            if (dis < toler && dis > 0.5)
            {
                nebor[j].append(i);
                nebor[i].append(j);
                BondLengthEntry entry;
                entry.end = i+1;
                entry.start = j+1;
                entry.length = dis;
                property.bondLengths.append(entry);
            }
        }
    }

    int uniq = 0;
    for(int b=0; b<ref.NumAtom(); ++b)
    {
        for(int i=0; i<nebor.value(b).size(); ++i)
        {
            int a = nebor.value(b).at(i);
            for(int j=0; j<nebor.value(b).size(); ++j)
            {
                int c = nebor.value(b).at(j);

                if (a==c)
                {
                    uniq = 1;
                    continue;
                }

                if (uniq)
                {
                    BondAngleEntry angle_entry;
                    angle_entry.start = a+1;
                    angle_entry.middle = b+1;
                    angle_entry.end = c+1;
                    angle_entry.angle = ref.getAngle(angle_entry.start, angle_entry.middle, angle_entry.end);
                    property.bondAngles.append(angle_entry);
                }

            }
            uniq = 0;
        }
    }

    int a, b, c, d;
    for(int i=0; i<property.bondLengths.size();++i)
    {
        const BondLengthEntry& bond = property.bondLengths.at(i);
        b = bond.start-1;
        c = bond.end-1;
        for(int k=0; k<nebor.value(b).size(); ++k)
        {
            a = nebor.value(b).at(k);
            if (a==c)
                continue;
            for(int j=0; j<nebor.value(c).size(); ++j)
            {
                d = nebor.value(c).at(j);
                if ((d==b) || (d==a))
                    continue;
                TorsionAngleEntry angle_entry;
                angle_entry.start = a+1;
                angle_entry.start_middle = b+1;
                angle_entry.middle_end = c+1;
                angle_entry.end = d+1;

                angle_entry.angle = ref.getTorsion(angle_entry.start, angle_entry.start_middle, angle_entry.middle_end, angle_entry.end);
                property.torsionAngles.append(angle_entry);
            }
        }
    }


    //Hydrogen bond.

    QList<BondLengthEntry> HBs;
    QSet<QPair<int, int> > visited;
    const QStringList HydrogenBondingElements = {"F", "N", "O"};
    const double HB_R_CUTOFF = 3.15;
    const double HB_R_MIN = 1.5;
    const double HB_A_CUTOFF = 25.0;

    for(int i=0; i<property.bondLengths.size();++i)
    {
        int s1 = property.bondLengths.at(i).start-1;
        int e1 = property.bondLengths.at(i).end-1;

        QString elem_s1 = ADPT::AtomicProperties::fromSymbol(ref.getElements().at(s1))->getSymbol();
        QString elem_e1 = ADPT::AtomicProperties::fromSymbol(ref.getElements().at(e1))->getSymbol();

        bool HBE1 = HydrogenBondingElements.contains(elem_s1);
        bool HBE2 = HydrogenBondingElements.contains(elem_e1);
        if ( !HBE1 && !HBE2 )
        {
            continue;
        }
        int A = -1;

        if (HBE1)
        {
            A = s1;
        }
        else if (HBE2)
        {
            A = e1;
        }


        for(int j=0; j<property.bondLengths.size(); ++j)
        {
            int s2 = property.bondLengths.at(j).start-1;
            int e2 = property.bondLengths.at(j).end-1;


            int H = -1;
            int D = -1;

            bool validHB = false;
            QString elem_s2 = ADPT::AtomicProperties::fromSymbol(ref.getElements().at(s2))->getSymbol();
            QString elem_e2 = ADPT::AtomicProperties::fromSymbol(ref.getElements().at(e2))->getSymbol();

            if (elem_e2 == "H")
            {
                bool HBE3 = HydrogenBondingElements.contains(elem_s2);
                if (HBE3)
                {
                    H = e2;
                    D = s2;
                    validHB = true;
                }
            }
            else if (elem_s2 == "H")
            {
                bool HBE3 = HydrogenBondingElements.contains(elem_e2);
                if (HBE3)
                {
                    H = s2;
                    D = e2;
                    validHB = true;
                }
            }

            if (D==A)
            {
                continue;
            }

            for(int k=0; k<nebor[A].size();++k)
            {
                if (nebor[A][k] == D)
                {
                    validHB = false;
                    break;
                }
            }


            if (validHB)
            {
                double R = ref.getDistance(A+1,H+1);
                double ang = ref.getAngle(A+1, D+1, H+1);
                if (R >= HB_R_MIN && R <= HB_R_CUTOFF && ang < HB_A_CUTOFF)
                {

                    BondLengthEntry entry;
                    entry.start = A+1;
                    entry.end = H+1;
                    entry.length = R;

                    auto pair = qMakePair(A+1, H+1);
                    if (!visited.contains(pair))
                    {
//                        qDebug() << "Find HB " << entry.start << " " << entry.end << " " << R ;
                        visited.insert(pair);
                        bool uniqueHB = true;
                        for(int k=0; k<property.nonCovalentBondLengths.size();++k)
                        {
                            if (property.nonCovalentBondLengths[k].start == entry.start
                                    && property.nonCovalentBondLengths[k].end == entry.end)
                            {
                                uniqueHB = false;
                                break;
                            }
                        }
                        if (uniqueHB)
                            HBs.append(entry);
                    }
                }
            }
        }
    }
    property.nonCovalentBondLengths.append(HBs);
}
void CalcAtomizationEnergy(MolecularData& molecularData,const QMap<QString, AtomicData>& atomicDataSet)
{
    if (!molecularData.totalEnergyData.isValid)
    {
        return;
    }
    double energyAtoms = 0.0;

    //make sure molecularData has geometryData
    QList<QString> elements = molecularData.geometryData.geometry.getElements();

    molecularData.atomizationEnergyData.isValid = true;
    for(int j=0; j<elements.size();++j)
    {
        if (!atomicDataSet.contains(elements[j]))
        {
            molecularData.atomizationEnergyData.isValid = false;
            break;
        }
        energyAtoms += atomicDataSet[elements[j]].energy;
        //qDebug() << "Energy for element " << elements[j] << " is " << atomicDataSet[elements[j]].energy;
    }

//    qDebug() << "sum of atom energy: " << energyAtoms;
//    qDebug() << "total energy" << molecularData.totalEnergyData.energy;
    molecularData.atomizationEnergyData.method = "DFTB";
    molecularData.atomizationEnergyData.energy = (energyAtoms - molecularData.totalEnergyData.energy);
    molecularData.atomizationEnergyData.energy_unit = "H";
}

}
