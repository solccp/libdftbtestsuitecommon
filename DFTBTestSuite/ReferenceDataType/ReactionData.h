#ifndef REACTIONDATA_H
#define REACTIONDATA_H

#include "basic_types.h"

#include <QString>
#include <QList>

namespace DFTBTestSuite
{

struct ReactionItem
{
    ReactionItem(){}
    ReactionItem(double coeff, const QString& uuid)
    {
        coefficient = coeff;
        geom_uuid = uuid;
    }
    double coefficient;
    ADPT::UUID geom_uuid;
};

struct ReferenceReactionData
{
    ADPT::UUID uuid;
    QString name;
    double energy;
    QString energy_unit = "kcal/mol";
    QString method;
    QString basis;
    QList<ReactionItem> reactants;
    QList<ReactionItem> products;
};

}

namespace YAML
{

}

#endif // REACTIONDATA_H
