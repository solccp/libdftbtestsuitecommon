#ifndef CRYSTALDATA_H
#define CRYSTALDATA_H


#include "basic_types.h"

#include <QVariant>
#include <QVector>
#include <array>
#include <QTextStream>
#include <QStringList>


namespace DFTBTestSuite
{


struct BandStructureTestEntry
{
    int bandIndex_start = 0;
    int bandIndex_end = -1;
    int pointIndex_start = 0;
    int pointIndex_end = -1;
    double weight = 1.0;
    QString type = "index_based";
    double lower_energy;
    double upper_energy;
};


struct AtomCoordinate
{
    AtomCoordinate(){}
    AtomCoordinate(const QString& sym, double x, double y, double z)
    {
        symbol = sym;
        position[0] = x;
        position[1] = y;
        position[2] = z;
    }

    QString symbol;
    dVector3 position;
};

class CrystalEnergyData
{
public:
    QString name;
    QString method = "unknown";
    QString basis = "unknown";
    ADPT::UUID uuid;
    double init_energy;
    double init_fermi_energy;
    double init_upe;
    double final_energy;
    double final_fermi_energy;
    double final_upe;
    bool opt_upe = false;
    bool spinpol = false;

    QString energy_unit = "H";
    QString path;
};


class CrystalGeometryData
{
public:
    QString name;
    QString method = "unknown";
    QString basis = "unknown";
    int charge=0;
    ADPT::UUID uuid;

    ADPT::KPointsSetting kpoints;

    double scaling_factor = 1.0;

    dMatrix3x3 lattice_vectors;

    bool fractional_coordinates;
    QList<AtomCoordinate> coordinates;

    QList<QString> getElements() const
    {
        QMap<QString, int> count;
        for(int i=0; i<coordinates.size();++i)
        {
            if (!count.contains(coordinates[i].symbol))
            {
                count[coordinates[i].symbol] = 1;
            }
            else
            {
                count[coordinates[i].symbol]++;
            }
        }
        return count.keys();
    }


    QString getFormula() const
    {
        QMap<QString, int> count;
        for(int i=0; i<coordinates.size();++i)
        {
            if (!count.contains(coordinates[i].symbol))
            {
                count[coordinates[i].symbol] = 1;
            }
            else
            {
                count[coordinates[i].symbol]++;
            }
        }
        QMapIterator<QString, int> it(count);
        QString res;
        while(it.hasNext())
        {
            it.next();
            res += QString("%1%2").arg(it.key()).arg(it.value());
        }
        return res;
    }

    QString toGen() const;

    double upe = 0.0;
    bool spinpol = false;
    bool opt_upe = true;
};

struct KLineEntry
{
    int nPoints;
    dVector3 position;
};

class BandStructureData
{
public:
    ADPT::UUID uuid;
    QString name;
    QString method = "unknown";
    QString basis = "unknown";
    ADPT::UUID geom_uuid;
    QVector<QVector<double> > bands;
    QVector<QVector<double> > bands_beta;


    ADPT::KPointsSetting kpoints;
    QPair<int, int> fermiIndex = {-1, -1};

    QList<BandStructureTestEntry> defaultTestRegion;
    void shiftBands(double value);
};

typedef QList<CrystalGeometryData> CrystalGeometryDataSet ;
typedef QList<BandStructureData> BandStructureDataSet ;

class CrystalData
{
public:
    CrystalGeometryDataSet geometryData;
    BandStructureDataSet bandstructureData;
};

}



#endif // CRYSTALDATA_H
