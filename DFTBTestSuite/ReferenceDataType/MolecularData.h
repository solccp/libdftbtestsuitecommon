#ifndef MOLECULARDATA_H
#define MOLECULARDATA_H

#include <QMap>
#include <QDateTime>
#include <QString>
#include <QList>
#include <iostream>
#include "moleculegeometry.h"
#include "AtomicData.h"
#include "basic_types.h"

const double HF__KCALMOL = 627.509469;
const double CM_1__KCAL_MOL = 0.00285911;

namespace DFTBTestSuite
{

struct BondLengthEntry
{
    int start;
    int end;
    double length;
};
struct BondAngleEntry
{
    int start;
    int middle;
    int end;
    double angle;
};

struct TorsionAngleEntry
{
    int start;
    int start_middle;
    int middle_end;
    int end;
    double angle;
};

struct GeometryProperty
{
    QList<BondLengthEntry> bondLengths;
    QList<BondAngleEntry> bondAngles;
    QList<TorsionAngleEntry> torsionAngles;
    QList<BondLengthEntry> nonCovalentBondLengths;
};


class GeometryData
{  
public:
    QString method;
    QString basisSet;
    bool optimized = false;
    MoleculeGeometry geometry;
};

void calcGeometryProperties(const MoleculeGeometry& ref, GeometryProperty& property);

struct FrequencyData
{
    bool isValid = false;
    QString method;
    QString basisSet;
    QList<double> frequencies;
    QList<double> weights;
    double ZPE = 0.0;
};

struct TotalEnergyData
{
    bool isValid = false;
    bool isOptimized = true;
    QString method;
    QString basisSet;
    double energy = 0.0;
    double optEnergy = 0.0;
    QString energy_unit = "H";
};
struct AtomizationEnergyData
{
    bool isValid = false;
    QString method;
    QString basisSet;
    double energy = 0.0;
    QString energy_unit = "kcal/mol";
};

class MolecularData
{
public:
    QString name;
    ADPT::UUID uuid;
    int charge = 0;
    int spin = 1;
    GeometryData geometryData;
    TotalEnergyData totalEnergyData;
    AtomizationEnergyData atomizationEnergyData;
    FrequencyData frequencyData;       
};

    void CalcAtomizationEnergy(DFTBTestSuite::MolecularData& molecularData,const QMap<QString, DFTBTestSuite::AtomicData>& atomicDataSet);

}

inline double CalcZPE(const QList<double>& freqs)
{
    double res = 0.0;
    int size = freqs.size();
    for(int i=0; i<size; ++i)
    {
        res += freqs[i];
    }

    //cm-1 to kcal/mol;
    res *= (0.5*CM_1__KCAL_MOL);
    return res;
}







#endif // MOLECULARDATA_H
