#ifndef MOLECULEGEOMETRY_H
#define MOLECULEGEOMETRY_H
#include <string>
#include <array>
#include <QList>
#include <QString>
#include <QStringList>
#include <vector>

namespace DFTBTestSuite
{

class MoleculeGeometry
{
public:
    MoleculeGeometry();
    bool readXYZString(const QString& geometry);
    int NumAtom() const;
    double getDistance(int index1, int index2) const;
    double getAngle(int index1, int middle, int index3) const;
    double getTorsion(int index1, int index2, int index3, int index4) const;
    void setCoord(int atomIndex, int coordIndex, double value);

    bool isLinear() const;
    void clear();
    void addAtom(const QString &element, double x, double y, double z);
    QString getFormula() const;

    QString toXYZ() const;
    QString toNoHeaderXYZ() const;
    QString toGen() const;
    const QList<QString>& getElements() const;
    QString getAtomSymbol(int index) const;
    std::array<double,3> getCoord(int index) const;
private:
    double getAngle_impl(const std::array<double, 3>& v1, const std::array<double,3>& v2) const;
    std::array<double, 3> vectorSubstract(const std::array<double, 3>& v1, const std::array<double,3>& v2) const;
    std::array<double, 3> vectorCross(const std::array<double, 3>& v1, const std::array<double,3>& v2)const ;
    double vectorLength(const std::array<double, 3>& v) const;
    double vectorDot(const std::array<double, 3>& v1, const std::array<double, 3>& v2) const;
    QList<QString> atom_symbols;
    QList<std::array<double,3> > coords;
};

}


#endif // MOLECULEGEOMETRY_H
