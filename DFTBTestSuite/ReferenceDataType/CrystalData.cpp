#include "CrystalData.h"

namespace DFTBTestSuite
{

QString CrystalGeometryData::toGen() const
{
    QByteArray array;
    QTextStream stream(&array);
    QStringList elements;
    for(int i=0; i<coordinates.size();++i)
    {
        elements.append(coordinates[i].symbol);
    }
    elements.removeDuplicates();

    if (fractional_coordinates)
    {
        stream << coordinates.size() << " F" << endl;
    }
    else
    {
        stream << coordinates.size() << " S" << endl;
    }
    stream << elements.join(" ") << endl;

    dMatrix3x3 latticeVector_final = {{{0,0,0}, {0,0,0},{0,0,0}}};


    for(uint i=0; i<3; ++i)
    {
        for(uint j=0; j<3; ++j)
        {
            latticeVector_final[i][j] = scaling_factor*lattice_vectors[i][j];
        }
    }

    for(int i=0; i<coordinates.size();++i)
    {
        int index = elements.indexOf(coordinates[i].symbol);

        stream << i+1 << " " << index+1 ;

        stream << QString("%1 %2 %3")
                .arg(coordinates[i].position[0],20,'f',12)
                .arg(coordinates[i].position[1],20,'f',12)
                .arg(coordinates[i].position[2],20,'f',12)
                << endl;
    }
    stream << "0.0 0.0 0.0" << endl;
    for(uint i=0; i<3; ++i)
    {
        stream << QString("%1 %2 %3")
                .arg(latticeVector_final[i][0],20,'f',12)
                .arg(latticeVector_final[i][1],20,'f',12)
                .arg(latticeVector_final[i][2],20,'f',12)
                << endl;
    }

    stream.flush();
    return QString(array);

}

void BandStructureData::shiftBands(double value)
{
    for(int i=0; i<bands.size(); ++i)
    {
        for(int j=0; j<bands[i].size();++j)
        {
            bands[i][j] -= value;
        }
    }
}

}
