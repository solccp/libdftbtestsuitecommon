#ifndef DFTBTESTSUITEINPUT_JSON_H
#define DFTBTESTSUITEINPUT_JSON_H

#include <Variant/Variant.h>

#include "variantqt.h"
#include "InputFile.h"
#include "ReferenceDataType/referencedata_variant.h"

namespace libvariant
{

    template<>
    struct VariantConvertor<DFTBTestSuite::Control>
    {
        static Variant toVariant(const DFTBTestSuite::Control &rhs)
        {
            Variant var;
            var["save_intermediate"] = rhs.CopyIntermediateFiles;
            var["toolchain_type"] = rhs.DFTBCodeType.toStdString();
            var["toolchain_path"] = rhs.DFTBPath.toStdString();
            var["report_format"] = rhs.ReportFormatter.toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::Control &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.CopyIntermediateFiles, var, "save_intermediate", bool, hasValue);
            GET_VARIANT(rhs.DFTBCodeType, var, "toolchain_type", QString);
            GET_VARIANT(rhs.DFTBPath, var, "toolchain_path", QString);
            GET_VARIANT_OPT(rhs.ReportFormatter, var, "report_format", QString, hasValue);
            GET_VARIANT_OPT(rhs.ScratchDir, var, "scratch_dir", QString, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::DFTBInput>
    {
        static Variant toVariant(const DFTBTestSuite::DFTBInput &rhs)
        {
            Variant var;
            var["template_energy"] = rhs.template_energy.toStdString();
            var["template_frequency"] = rhs.template_frequency.toStdString();
            var["template_opt"] = rhs.template_opt.toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::DFTBInput &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.template_energy, var, "template_energy", QString, hasValue);
            GET_VARIANT_OPT(rhs.template_frequency, var, "template_frequency", QString, hasValue);
            GET_VARIANT_OPT(rhs.template_opt, var, "template_opt", QString, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::Dispersion>
    {
        static Variant toVariant(const DFTBTestSuite::Dispersion &rhs)
        {
            Variant var;
            if (!rhs.m_ljdispersion.isEmpty())
            {
                var["lennard_jones"] = VariantConvertor<QMap<QString, DFTBTestSuite::LJDispersionEntry> >::toVariant(rhs.m_ljdispersion);
            }
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::Dispersion &rhs)
        {
            using map = QMap<QString, DFTBTestSuite::LJDispersionEntry>;
            bool hasValue;
            GET_VARIANT_OPT(rhs.m_ljdispersion, var, "lennard_jones", map, hasValue);
            Q_UNUSED(hasValue);
        }
    };



    template<>
    struct VariantConvertor<DFTBTestSuite::AtomicEnergy>
    {
        static Variant toVariant(const DFTBTestSuite::AtomicEnergy &rhs)
        {
            std::map<std::string, Variant> var;
            {
                QMapIterator<QString, QString> it(rhs.atomic_dftbinputs);
                while(it.hasNext())
                {
                    it.next();
                    var[it.key().toStdString()] = it.value().toStdString();
                }
            }
            {
                QMapIterator<QString, double> it(rhs.atomic_energy);
                while(it.hasNext())
                {
                    it.next();
                    var[it.key().toStdString()] = it.value();
                }
            }
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::AtomicEnergy &rhs)
        {
            try
            {
                for(auto it = var.MapBegin(); it != var.MapEnd(); ++it)
                {
                    QString key = QString::fromStdString(it->first);
                    QVariant tmp = QVariant(QString::fromStdString(it->second.AsString()));
                    bool ok;
                    double value = tmp.toDouble(&ok);
                    if (ok)
                    {
                        rhs.atomic_energy.insert(key, value);
                    }
                    else
                    {
                        rhs.atomic_dftbinputs.insert(key, tmp.toString());
                    }
                }

            }
            catch (const std::exception& e)
            {
                std::cout << e.what() << std::endl;
                throw e;
            }
        }
    };



    template<>
    struct VariantConvertor<DFTBTestSuite::BandStructureTarget>
    {
        static Variant toVariant(const DFTBTestSuite::BandStructureTarget &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            var["template_input"] = VariantConvertor<DFTBTestSuite::DFTBInput>::toVariant(rhs.template_input);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::BandStructureTarget &rhs)
        {

                GET_VARIANT(rhs.name, var, "name", QString);
                GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);

                bool hasValue;
                GET_VARIANT_OPT(rhs.template_input, var, "template_input", DFTBTestSuite::DFTBInput, hasValue);
                Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::EvaluationCrystal>
    {
        static Variant toVariant(const DFTBTestSuite::EvaluationCrystal &rhs)
        {
            Variant var;
            var["bandstructure"] = VariantConvertor<QList<DFTBTestSuite::BandStructureTarget> >::toVariant(rhs.bandstructureTargets);
            var["crystal"] = VariantConvertor<QList<DFTBTestSuite::CrystalStructureTarget> >::toVariant(rhs.crystalstructureTargets);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::EvaluationCrystal &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.bandstructureTargets, var, "bandstructure", QList<DFTBTestSuite::BandStructureTarget>, hasValue);
            GET_VARIANT_OPT(rhs.crystalstructureTargets, var, "crystal", QList<DFTBTestSuite::CrystalStructureTarget>, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::EvaluationMolecule>
    {
        static Variant toVariant(const DFTBTestSuite::EvaluationMolecule &rhs)
        {
            Variant var;
            var["atomic_energy"] = VariantConvertor<DFTBTestSuite::AtomicEnergy>::toVariant(rhs.atomicEnergy);
            var["molecule"] = VariantConvertor<QList<DFTBTestSuite::MoleculeTarget> >::toVariant(rhs.moleculeTargets);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::EvaluationMolecule &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.atomicEnergy, var, "atomic_energy", DFTBTestSuite::AtomicEnergy, hasValue);
            GET_VARIANT_OPT(rhs.moleculeTargets, var, "molecule", QList<DFTBTestSuite::MoleculeTarget>, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::EvaluationReaction>
    {
        static Variant toVariant(const DFTBTestSuite::EvaluationReaction &rhs)
        {
            Variant var;
            var["reaction"] = VariantConvertor<QList<DFTBTestSuite::ReactionTarget> >::toVariant(rhs.reactionTargets);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::EvaluationReaction &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.reactionTargets, var, "reaction", QList<DFTBTestSuite::ReactionTarget>, hasValue);
            Q_UNUSED(hasValue);
        }
    };



    template<>
    struct VariantConvertor<DFTBTestSuite::TestBandStructure>
    {
        static Variant toVariant(const DFTBTestSuite::TestBandStructure &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            for(int i=0; i<rhs.testRegion.size();++i)
            {
                var["test_region"].Append(VariantConvertor<DFTBTestSuite::BandStructureTestEntry>::toVariant(rhs.testRegion[i]));
            }
            var["weight"]  = rhs.weight;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::TestBandStructure &rhs)
        {
            GET_VARIANT(rhs.name, var, "name", QString);
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
            GET_VARIANT(rhs.testRegion, var, "test_region", QList<DFTBTestSuite::BandStructureTestEntry>);
            bool hasValue;
            GET_VARIANT_OPT(rhs.weight, var, "weight", double, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::PropertyTester_Input>
    {
        static Variant toVariant(const DFTBTestSuite::PropertyTester_Input &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["weight"]  = rhs.weight;
            var["options"] = VariantConvertor<QVariantMap>::toVariant(rhs.options);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::PropertyTester_Input &rhs)
        {
            GET_VARIANT(rhs.name, var, "name", QString);
            bool hasValue;
            GET_VARIANT_OPT(rhs.weight, var, "weight", double, hasValue);
            GET_VARIANT_OPT(rhs.options, var, "options", QVariantMap, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::TestMolecule>
    {
        static Variant toVariant(const DFTBTestSuite::TestMolecule &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            var["testers"] = VariantConvertor<QList<DFTBTestSuite::PropertyTester_Input> >::toVariant(rhs.testers);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::TestMolecule &rhs)
        {
            GET_VARIANT(rhs.name, var, "name", QString);
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
            GET_VARIANT(rhs.testers, var, "testers", QList<DFTBTestSuite::PropertyTester_Input>);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::TestReaction>
    {
        static Variant toVariant(const DFTBTestSuite::TestReaction &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            var["weight"]  = rhs.weight;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::TestReaction &rhs)
        {
            GET_VARIANT(rhs.name, var, "name", QString);
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
            GET_VARIANT(rhs.weight, var, "weight", double);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::ErrorUnit>
    {
        static Variant toVariant(const DFTBTestSuite::ErrorUnit &rhs)
        {
            Variant var;
            var["atomization_energy"] = rhs.AtomizationEnergy;
            var["bandstructure"] = rhs.BandStructure;
            var["bond_angle"] = rhs.BondAngle;
            var["bond_length"] = rhs.BondLength;
            if (rhs.useFrequencyPercentage)
            {
                var["frequency_percentage"] = rhs.Frequency_Percentage;
            }
            else
            {
                var["frequency"] = rhs.Frequency;
            }
            var["reaction_energy"] = rhs.ReactionEnergy;
            var["torsion_angle"] = rhs.TorsionAngle;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::ErrorUnit &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.AtomizationEnergy, var, "atomization_energy", double, hasValue);
            GET_VARIANT_OPT(rhs.BandStructure, var, "bandstructure", double, hasValue);
            GET_VARIANT_OPT(rhs.BondAngle, var, "bond_angle", double, hasValue);
            GET_VARIANT_OPT(rhs.BondLength, var, "bond_length", double, hasValue);
            GET_VARIANT_OPT(rhs.ReactionEnergy, var, "reaction_energy", double, hasValue);
            GET_VARIANT_OPT(rhs.TorsionAngle, var, "torsion_angle", double, hasValue);
            GET_VARIANT_OPT(rhs.Frequency_Percentage, var, "frequency_percentage", double, rhs.useFrequencyPercentage);
            GET_VARIANT_OPT(rhs.Frequency, var, "frequency", double, hasValue);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::LJDispersionEntry>
    {
        static Variant toVariant(const DFTBTestSuite::LJDispersionEntry &rhs)
        {
            Variant var;
            var["distance"] = rhs.distance;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::LJDispersionEntry &rhs)
        {
            GET_VARIANT(rhs.distance, var, "distance", double);
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::CrystalSpinPolarizationSettings>
    {
        static Variant toVariant(const DFTBTestSuite::CrystalSpinPolarizationSettings &rhs)
        {
            Variant var;
            var["spinpol"] = rhs.spinpol;
            var["upe"] = rhs.upe;
            var["opt_upe"] = rhs.opt_upe;
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::CrystalSpinPolarizationSettings &rhs)
        {
            GET_VARIANT(rhs.spinpol, var, "spinpol", bool);
            if (rhs.spinpol)
            {
                GET_VARIANT(rhs.opt_upe, var, "opt_upe", bool);
                GET_VARIANT_OPT(rhs.upe, var, "upe", double, rhs.has_upe);
            }
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::CrystalStructureTarget>
    {
        static Variant toVariant(const DFTBTestSuite::CrystalStructureTarget &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            var["template_input"] = VariantConvertor<DFTBTestSuite::DFTBInput>::toVariant(rhs.template_input);
            if (rhs.kpoints.numOfPoints() > 0)
            {
                var["kpoints"] = VariantConvertor<ADPT::KPointsSetting>::toVariant(rhs.kpoints);
            }
            if (rhs.hasSpinPolarSetting)
            {
                var["spinpol_setting"] = VariantConvertor<DFTBTestSuite::CrystalSpinPolarizationSettings>::toVariant(rhs.spinpol_setting);
            }
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::CrystalStructureTarget &rhs)
        {
            GET_VARIANT(rhs.name, var, "name", QString);
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
            bool hasValue = false;
            GET_VARIANT_OPT(rhs.template_input, var, "template_input", DFTBTestSuite::DFTBInput, hasValue);
            GET_VARIANT_OPT(rhs.kpoints, var, "kpoints", ADPT::KPointsSetting, rhs.hasKpointsetting);
            GET_VARIANT_OPT(rhs.spinpol_setting, var, "spinpol_setting", DFTBTestSuite::CrystalSpinPolarizationSettings, rhs.hasSpinPolarSetting);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::MoleculeTarget>
    {
        static Variant toVariant(const DFTBTestSuite::MoleculeTarget &rhs)
        {
            Variant var;
            var["name"] = rhs.name.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            var["template_input"] = VariantConvertor<DFTBTestSuite::DFTBInput>::toVariant(rhs.template_input);
            var["evaluation_level"] = rhs.modeToString(rhs.evalutationLevel).toLower().toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::MoleculeTarget &rhs)
        {
            GET_VARIANT(rhs.name, var, "name", QString);
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
            bool hasValue;
            GET_VARIANT_OPT(rhs.template_input, var, "template_input", DFTBTestSuite::DFTBInput, hasValue);
            QString eva_level;
            GET_VARIANT(eva_level, var, "evaluation_level", QString);
            rhs.evalutationLevel = rhs.modeFromString(eva_level);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<DFTBTestSuite::ReactionTarget>
    {
        static Variant toVariant(const DFTBTestSuite::ReactionTarget &rhs)
        {
            Variant var;
            var["optimize_components"] = rhs.GeomOptComponents;
            var["name"] = rhs.name.toStdString();
            var["uuid"] = VariantConvertor<ADPT::UUID>::toVariant(rhs.uuid);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::ReactionTarget &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.GeomOptComponents, var, "optimize_components", bool, hasValue);
            GET_VARIANT(rhs.name, var, "name", QString);
            GET_VARIANT(rhs.uuid, var, "uuid", ADPT::UUID);
            Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<DFTBTestSuite::TestingOption>
    {
        static Variant toVariant(const DFTBTestSuite::TestingOption &rhs)
        {
            Variant var;
            var["punish_imaginary_frequency"] = rhs.punishImaginaryFrequency;
            var["max_error_ratio"] = rhs.maxErrorRatio;
            var["error_unit"] = VariantConvertor<DFTBTestSuite::ErrorUnit>::toVariant(rhs.errorUnit);
            var["bandstructure"] = VariantConvertor<QList<DFTBTestSuite::TestBandStructure> >::toVariant(rhs.testBandStructure);
            var["molecule"] = VariantConvertor<QList<DFTBTestSuite::TestMolecule> >::toVariant(rhs.testMolecules);
            var["reaction"] = VariantConvertor<QList<DFTBTestSuite::TestReaction> >::toVariant(rhs.testReactions);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::TestingOption &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.punishImaginaryFrequency, var, "punish_imaginary_frequency", bool, hasValue);
            GET_VARIANT_OPT(rhs.maxErrorRatio, var, "max_error_ratio", double, hasValue);
            GET_VARIANT_OPT(rhs.errorUnit, var, "error_unit", DFTBTestSuite::ErrorUnit, hasValue);
            GET_VARIANT_OPT(rhs.testBandStructure, var, "bandstructure", QList<DFTBTestSuite::TestBandStructure>, hasValue);
            GET_VARIANT_OPT(rhs.testMolecules, var, "molecule", QList<DFTBTestSuite::TestMolecule> , hasValue);
            GET_VARIANT_OPT(rhs.testReactions, var, "reaction", QList<DFTBTestSuite::TestReaction>, hasValue);
            Q_UNUSED(hasValue);
        }
    };



    template<>
    struct VariantConvertor<DFTBTestSuite::Evaluation>
    {
        static Variant toVariant(const DFTBTestSuite::Evaluation &rhs)
        {
            Variant var;

            if (rhs.useDampingFactor)
            {
                var["damping"] = rhs.dampingFactor;
            }

            var["timeout"] = rhs.timeout;
            var["crystal_settings"] = VariantConvertor<DFTBTestSuite::EvaluationCrystal>::toVariant(rhs.evaluationCrystal);
            var["molecule_settings"] = VariantConvertor<DFTBTestSuite::EvaluationMolecule>::toVariant(rhs.evaluationMolecule);
            var["reaction_settings"] = VariantConvertor<DFTBTestSuite::EvaluationReaction>::toVariant(rhs.evaluationReaction);


            Variant temp_input;
            temp_input["crystal"] = VariantConvertor<DFTBTestSuite::DFTBInput>::toVariant(rhs.crystal_default_dftbinput);
            temp_input["molecule"] = VariantConvertor<DFTBTestSuite::DFTBInput>::toVariant(rhs.mole_default_dftbinput);
            var["template_inputfile"] = temp_input;

            if (rhs.SKInfo)
            {
                var["slater_koster_files"] = VariantConvertor<ADPT::SKFileInfo>::toVariant(*rhs.SKInfo);
            }

            if (!rhs.dftb_options.isEmpty())
            {
                var["dftb_options"] = VariantConvertor<QVariantMap>::toVariant(rhs.dftb_options);
            }

            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::Evaluation &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.dampingFactor, var, "damping", double, rhs.useDampingFactor);

            GET_VARIANT_OPT(rhs.timeout, var, "timeout", int, hasValue);
            GET_VARIANT_OPT(rhs.evaluationCrystal, var, "crystal_settings", DFTBTestSuite::EvaluationCrystal, hasValue);
            GET_VARIANT_OPT(rhs.evaluationMolecule, var, "molecule_settings", DFTBTestSuite::EvaluationMolecule, hasValue);
            GET_VARIANT_OPT(rhs.evaluationReaction, var, "reaction_settings", DFTBTestSuite::EvaluationReaction, hasValue);

            typedef QMap<QString, double> msd;
            GET_VARIANT_PTR_OPT(rhs.SKInfo, var, "slater_koster_files", ADPT::SKFileInfo, hasValue);
            GET_VARIANT_OPT(rhs.mole_default_dftbinput, var["template_inputfile"], "molecule", DFTBTestSuite::DFTBInput, hasValue);
            GET_VARIANT_OPT(rhs.crystal_default_dftbinput, var["template_inputfile"], "crystal", DFTBTestSuite::DFTBInput, hasValue);
            Q_UNUSED(hasValue);
            GET_VARIANT_OPT(rhs.dftb_options, var, "dftb_options", QVariantMap, hasValue);
        }
    };



    template<>
    struct VariantConvertor<DFTBTestSuite::InputData>
    {
        static Variant toVariant(const DFTBTestSuite::InputData &rhs)
        {
            Variant var;
            var["control"] = VariantConvertor<DFTBTestSuite::Control>::toVariant(rhs.control);
            var["evaluation"] = VariantConvertor<DFTBTestSuite::Evaluation>::toVariant(rhs.evaluation);
            var["testing"] = VariantConvertor<DFTBTestSuite::TestingOption>::toVariant(rhs.testingOption);
            return var;
        }
        static void fromVariant(const Variant &var, DFTBTestSuite::InputData &rhs)
        {
            GET_VARIANT(rhs.control, var, "control", DFTBTestSuite::Control);
            GET_VARIANT(rhs.evaluation, var, "evaluation", DFTBTestSuite::Evaluation);
            GET_VARIANT(rhs.testingOption, var, "testing", DFTBTestSuite::TestingOption);
        }
    };
}


#endif // DFTBTESTSUITEINPUT_JSON_H

