#include "util.h"
#include <QProcess>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QByteArray>
#include <QProcessEnvironment>
#include <QStringList>
#include <QDir>
#include <QFileInfo>

#include <stdexcept>
#include <fstream>

namespace DFTBTestSuite
{

//using namespace std;








QString XYZ2Gen(const QString& xyzstring)
{
   QStringList elements;
   QStringList result;
   QStringList geom;
   QStringList symbols;
   QTextStream fin(xyzstring.toLatin1());
   int nat = fin.readLine().toInt();
   fin.readLine();
   for(int i=0; i<nat; ++i)
   {
       QString line = fin.readLine();
       QStringList arr = line.split(" ", QString::SkipEmptyParts);
       if (!elements.contains(arr[0]))
       {
           elements.push_back(arr[0]);
       }
       symbols.push_back(arr[0]);
       arr.pop_front();
       geom.push_back(arr.join(" "));
   }
   result.push_back(QString("%1 C").arg(geom.size()));
   result.push_back(elements.join(" "));
   for(int i=0; i<geom.size();++i)
   {
       result.push_back(QString("%1 %2 %3").arg(i+1).arg(elements.indexOf(symbols[i])+1).arg(geom[i]));
   }

   return result.join("\n");
}


void checkFile(const QString& filename)
{
    QFileInfo info(filename);
    if (!info.exists() || !info.isReadable())
    {
        throw std::invalid_argument("  File '" + filename.toStdString() + "' is not found or not readable.");
    }
}

void insertFile(QTextStream& os, const QString& filename)
{
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream stream(&file);
        QString str;
        while(!stream.atEnd())
        {
            str = stream.readLine();
            os << str << endl;
        }
    }
}


bool writeFile(const QString &filename, const QString &content)
{
    QFile file(filename);
    if (file.open(QFile::WriteOnly))
    {
        QTextStream stream(&file);
        stream << content ;
        return true;
    }
    return false;
}

QPair<QString, QString> potential2ElementPair(const QString &potential)
{
    QStringList arr = potential.split("-", QString::SkipEmptyParts);
    QPair<QString, QString> res;
    res.first = arr[0];
    res.second = arr[1];
    return res;
}

QString switchPotential(const QString &potential)
{
    QStringList arr = potential.split("-", QString::SkipEmptyParts);
    return QString("%1-%2").arg(arr[1]).arg(arr[0]);
}

QString elementPair2Potential(const QPair<QString, QString> &pair)
{
    return QString("%1-%2").arg(pair.first).arg(pair.second);
}


}
