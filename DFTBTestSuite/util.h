#ifndef UTIL_H_DFTBTESTSUITE
#define UTIL_H_DFTBTESTSUITE
#include <QString>
#include <QStringList>
#include <QTextStream>

namespace DFTBTestSuite
{
    QString XYZ2Gen(const QString& xyzstring);
    void checkFile(const QString &filename);
    void insertFile(QTextStream& os, const QString& filename);

    template<class Container>
    int indexUUID(const Container& C, const QString& uuid)
    {
        for(int i=0; i<C.size(); ++i)
        {
            if (C[i].uuid.toString() == uuid)
            {
                return i;
            }
        }
        return -1;
    }

    bool writeFile(const QString &filename, const QString &content);

    QPair<QString, QString> potential2ElementPair(const QString& potential);
    QString switchPotential(const QString& potential);
    QString elementPair2Potential(const QPair<QString, QString>& pair);

}

#endif // UTIL_H_DFTBTESTSUITE
