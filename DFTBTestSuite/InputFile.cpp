#include "InputFile.h"
#include "util.h"
#include "ReferenceDataType/ReferenceData.h"
#include <QSet>
#include <QPair>

namespace DFTBTestSuite{

const PropertyTester_Input TestMolecule::MolecularGeometryTester = PropertyTester_Input("molecular_geometry");
const PropertyTester_Input TestMolecule::FrequencyTester = PropertyTester_Input("frequency");
const PropertyTester_Input TestMolecule::AtomizationEnergyTester = PropertyTester_Input("atomization_energy");

void makeAbsolutePath_local(const QDir& rootDir, QString& path)
{
    if (path.isEmpty())
        return;
    QFileInfo info(path);

    if (info.isAbsolute())
    {
        return;
    }
    else
    {
        QString file = ADPT::expandpath(path);
        if (QFileInfo(file).exists())
        {
            path = file;
        }
        else
        {
            path = rootDir.absoluteFilePath(path);
        }
    }
}

bool InputData::makeAbsolutePath(const QDir& rootDir)
{

    control.DFTBPath = ADPT::which(control.DFTBPath);
    control.DFTBPath = ADPT::expandpath(control.DFTBPath);


    if (!QFileInfo(control.ScratchDir).isAbsolute())
    {
        control.ScratchDir = ADPT::expandpath(control.ScratchDir);
    }
    makeAbsolutePath_local(rootDir, evaluation.mole_default_dftbinput.template_energy);
    makeAbsolutePath_local(rootDir, evaluation.mole_default_dftbinput.template_frequency);
    makeAbsolutePath_local(rootDir, evaluation.mole_default_dftbinput.template_opt);

    makeAbsolutePath_local(rootDir, evaluation.crystal_default_dftbinput.template_energy);
    makeAbsolutePath_local(rootDir, evaluation.crystal_default_dftbinput.template_frequency);
    makeAbsolutePath_local(rootDir, evaluation.crystal_default_dftbinput.template_opt);


    QMapIterator<QString, QString> it(evaluation.evaluationMolecule.atomicEnergy.atomic_dftbinputs);
    while(it.hasNext())
    {
        it.next();
        makeAbsolutePath_local(rootDir, evaluation.evaluationMolecule.atomicEnergy.atomic_dftbinputs[it.key()]);
    }

    for(int i=0; i< evaluation.evaluationMolecule.moleculeTargets.size(); ++i)
    {
        DFTBInput& input = evaluation.evaluationMolecule.moleculeTargets[i].template_input;
        makeAbsolutePath_local(rootDir, input.template_energy);
        makeAbsolutePath_local(rootDir, input.template_frequency);
        makeAbsolutePath_local(rootDir, input.template_opt);
    }

    for(int i=0; i< evaluation.evaluationCrystal.bandstructureTargets.size(); ++i)
    {
        DFTBInput& input = evaluation.evaluationCrystal.bandstructureTargets[i].template_input;
        makeAbsolutePath_local(rootDir, input.template_energy);
        makeAbsolutePath_local(rootDir, input.template_frequency);
        makeAbsolutePath_local(rootDir, input.template_opt);
    }

    if (evaluation.SKInfo)
    {
        evaluation.SKInfo->makeAbsolutePath(rootDir);
    }

    return true;
}

QSet<QString> InputData::required_rep_pairs(const ReferenceData& ref_data) const
{
    QSet<QString> res;

    //store all uuids for molecules
    QStringList uuids;

    for(int i=0; i< evaluation.evaluationMolecule.moleculeTargets.size(); ++i)
    {
        QString target = evaluation.evaluationMolecule.moleculeTargets[i].uuid.toString();
        if (evaluation.evaluationMolecule.moleculeTargets[i].evalutationLevel >= MoleculeTarget::Energy)
        {
            uuids.append(target);
        }
    }

// not required
//    for(int i=0; i< evaluation.evaluationCrystal.crystalstructureTargets.size(); ++i)
//    {
//        QString target = evaluation.evaluationCrystal.crystalstructureTargets[i].uuid.toString();
//        uuids.append(target);
//    }


    for(int i=0; i< evaluation.evaluationReaction.reactionTargets.size();++i)
    {
        QString uuid = evaluation.evaluationReaction.reactionTargets[i].uuid.toString();
        int index = indexUUID(ref_data.reactionDataSet, uuid);

        if (index < 0)
        {
            continue;
        }

        const ReferenceReactionData& reaction = ref_data.reactionDataSet[index];

        for(int j=0; j< reaction.reactants.size();++j)
        {
            uuids.append(reaction.reactants.at(j).geom_uuid.toString());
        }
        for(int j=0; j< reaction.products.size();++j)
        {
            uuids.append(reaction.products.at(j).geom_uuid.toString());
        }
   }

    for(int i=0; i< uuids.size();++i)
    {
        QStringList elements;
        QString target = uuids.at(i);

        int index = indexUUID(ref_data.crystalData.geometryData, target);

        if (ref_data.molecularDataIndexMapping.contains(target))
        {
            int index = ref_data.molecularDataIndexMapping[target];
            QStringList elems = ref_data.molecularDataSet[index].geometryData.geometry.getElements();
            elems.removeDuplicates();
            for(int j=0; j<elems.size();++j)
            {
                elements.append(elems[j]);
            }
            elements.removeDuplicates();
            elements.sort();
            for(int k=0; k<elements.size();++k)
            {
                for(int j=k; j<elements.size();++j)
                {
                    res.insert(QString("%1-%2").arg(elements.at(k)).arg(elements.at(j)));
                }
            }
        }
        else if (index > -1)
        {
            QStringList elems = ref_data.crystalData.geometryData[index].getElements();
            elems.removeDuplicates();
            for(int j=0; j<elems.size();++j)
            {
                elements.append(elems[j]);
            }
            elements.removeDuplicates();
            elements.sort();
            for(int k=0; k<elements.size();++k)
            {
                for(int j=k; j<elements.size();++j)
                {
                    res.insert(QString("%1-%2").arg(elements.at(k)).arg(elements.at(j)));
                }
            }
        }

    }

    //! The following part is potentially duplicated. check the entire member function.
    for(int i=0; i< testingOption.testMolecules.size(); ++i)
    {
        auto const& target = testingOption.testMolecules[i];
        if (!target.testers.isEmpty())
        {
            int index = indexUUID(ref_data.molecularDataSet, target.uuid.toString());
            if (index > -1)
            {
                auto elements = ref_data.molecularDataSet[index].geometryData.geometry.getElements();
                for(int k=0; k<elements.size();++k)
                {
                    for(int j=0; j<elements.size();++j)
                    {
                        res.insert(elementPair2Potential(qMakePair(elements.at(k), elements.at(j))));
                    }
                }
            }

        }
    }

    for(int i=0; i< testingOption.testReactions.size(); ++i)
    {
         auto const& target = testingOption.testReactions[i];
         int index = indexUUID(ref_data.reactionDataSet, target.uuid.toString());
         if (index > -1)
         {
            QList<QString> uuids;
            auto const & reaction = ref_data.reactionDataSet[index];
            foreach (auto const & item, reaction.products)
            {
                uuids.append(item.geom_uuid.toString());
            }
            foreach (auto const & item, reaction.reactants)
            {
                uuids.append(item.geom_uuid.toString());
            }
            foreach(auto const & uuid, uuids)
            {
                int geom_index = indexUUID(ref_data.molecularDataSet, uuid);
                if (geom_index > -1)
                {
                    auto elements = ref_data.molecularDataSet[geom_index].geometryData.geometry.getElements();
                    for(int k=0; k<elements.size();++k)
                    {
                        for(int j=0; j<elements.size();++j)
                        {
                            res.insert(elementPair2Potential(qMakePair(elements.at(k), elements.at(j))));
                        }
                    }
                }
            }
         }
    }

    return res;

}

QSet<QString> InputData::required_elec_pairs(const ReferenceData &ref_data) const
{
    QSet<QString> res = required_rep_pairs(ref_data);

    QSetIterator<QString> it(res);
    while(it.hasNext())
    {
        QString poten = it.next();
        QString switchedPotential = switchPotential(poten);
        res.insert(switchedPotential);
    }

    //Examination for Bandstructure;

    for(int i=0; i< testingOption.testBandStructure.size(); ++i)
    {
        const TestBandStructure& target = testingOption.testBandStructure[i];
        int index = indexUUID(ref_data.crystalData.bandstructureData, target.uuid.toString());
        int testBSIndex = indexUUID(testingOption.testBandStructure, target.uuid.toString());

        if (index == -1 || testBSIndex == -1)
        {
            continue;
        }

        if (testingOption.testBandStructure[testBSIndex].testRegion.isEmpty())
        {
            if (ref_data.crystalData.bandstructureData[index].defaultTestRegion.isEmpty())
            {
                continue;
            }
        }


        QString geom_uuid = ref_data.crystalData.bandstructureData[index].geom_uuid.toString();

        int geomIndex = indexUUID(ref_data.crystalData.geometryData, geom_uuid);
        if (geomIndex!= -1)
        {
            QStringList elements;
            for(int j=0; j<ref_data.crystalData.geometryData[geomIndex].coordinates.size();++j)
            {
                elements.append(ref_data.crystalData.geometryData[geomIndex].coordinates.at(j).symbol);
            }
            for(int k=0; k<elements.size();++k)
            {
                for(int j=0; j<elements.size();++j)
                {
                    res.insert(elementPair2Potential(qMakePair(elements.at(k), elements.at(j))));
                }
            }
        }

    }




    return res;
}


Evaluation::Evaluation(const Evaluation &rhs)
{
    if (rhs.SKInfo)
    {
        this->SKInfo = std::make_shared<ADPT::SKFileInfo>(*rhs.SKInfo);
    }

//    HubbardDerivs = rhs.HubbardDerivs;
    dftb_options = rhs.dftb_options;
    useDampingFactor = rhs.useDampingFactor;
    dampingFactor = rhs.dampingFactor;

    mole_default_dftbinput = rhs.mole_default_dftbinput;
    crystal_default_dftbinput = rhs.crystal_default_dftbinput;

    evaluationMolecule = rhs.evaluationMolecule;
    evaluationCrystal = rhs.evaluationCrystal;
    evaluationReaction = rhs.evaluationReaction;

}


}
