#ifndef INPUTFILE_H
#define INPUTFILE_H

#include "ReferenceDataType/ReactionData.h"
#include "ReferenceDataType/CrystalData.h"
#include "util.h"
#include "process_utils.h"
#include "basic_types.h"

#include <memory>
#include <array>

#include <QString>
#include <QDir>
#include <QMap>
#include <QDebug>


namespace DFTBTestSuite
{

struct ErrorUnit
{
    double BondLength        = 0.003;
    double BondAngle         = 1.0;
    double TorsionAngle      = 3.0;
    double AtomizationEnergy = 3.0;
    double ReactionEnergy    = 3.0;
    double Frequency         = 10;
    double BandStructure     = 0.05;
    double Frequency_Percentage = 3;
    bool useFrequencyPercentage = true;
};

class PropertyTester_Input
{
public:
    QString name;
    double weight = 1.0;
    QVariantMap options;
    PropertyTester_Input(){}
    PropertyTester_Input(const QString& name, double weight = 1.0, QVariantMap options = QVariantMap())
    {
        this->name = name;
        this->weight = weight;
        this->options = options;
    }
};

class TestMolecule
{
public:
    static const PropertyTester_Input MolecularGeometryTester;
    static const PropertyTester_Input FrequencyTester;
    static const PropertyTester_Input AtomizationEnergyTester;
    ADPT::UUID uuid;
    QString name;
    QList<PropertyTester_Input> testers;
};

class TestReaction
{
public:
    double weight = 1.0;
    ADPT::UUID uuid;
    QString name;
};

class DFTBInput
{
public:
    QString template_energy;
    QString template_opt;
    QString template_frequency;
    DFTBInput& operator=(const DFTBInput& rhs)
    {
        if (!rhs.template_energy.isEmpty())
        {
            template_energy = rhs.template_energy;
        }
        if (!rhs.template_opt.isEmpty())
        {
            template_opt = rhs.template_opt;
        }
        if (!rhs.template_frequency.isEmpty())
        {
            template_frequency = rhs.template_frequency;
        }
        return *this;
    }
};

class MoleculeTarget
{
public:
    enum EvaluationLevel{
        None = 0,
        Energy = 1,
        Geometry = 2,
        Frequency = 4
    };

    ADPT::UUID uuid;
    QString name;

    EvaluationLevel evalutationLevel = Frequency;
    DFTBInput template_input;

    static QString modeToString(EvaluationLevel mode)
    {
        if (mode == Frequency)
        {
            return "Frequency";
        }
        else if (mode == Geometry)
        {
            return "Geometry";
        }
        else if (mode == Energy)
        {
            return "Energy";
        }
        else
            return "None";
    }
    static EvaluationLevel modeFromString(const QString& str)
    {
        if (str.toLower() == "frequency")
        {
            return Frequency;
        }
        else if (str.toLower() == "geometry")
        {
            return Geometry;
        }
        else if (str.toLower() == "energy")
        {
            return Energy;
        }
        else
            return None;
    }
};


class AtomicEnergy
{
public:
    QMap<QString, QString> atomic_dftbinputs;
    QMap<QString, double> atomic_energy;
};

struct EvaluationMolecule
{
    AtomicEnergy atomicEnergy;
    QList<MoleculeTarget> moleculeTargets;
};

struct CrystalSpinPolarizationSettings
{
    bool spinpol = false;
    bool opt_upe = true;
    bool has_upe = false;
    double upe = 0.0;
};

struct CrystalStructureTarget
{
    QString name;
    ADPT::UUID uuid;
    DFTBInput template_input;

    bool hasKpointsetting = false;
    ADPT::KPointsSetting kpoints;
    bool hasSpinPolarSetting = false;
    CrystalSpinPolarizationSettings spinpol_setting;
};

struct BandStructureTarget
{
    ADPT::UUID uuid;
    QString name;
    DFTBInput template_input;
};

struct ReactionTarget
{
    QString name;
    ADPT::UUID uuid;
    bool GeomOptComponents = false;
};

struct EvaluationCrystal
{
    QList<CrystalStructureTarget> crystalstructureTargets;
    QList<BandStructureTarget> bandstructureTargets;
};

struct EvaluationReaction
{
    QList<ReactionTarget> reactionTargets;
};



struct Control
{
    QString ScratchDir  = QDir::tempPath();
    QString DFTBCodeType = "dftb+";
    QString DFTBPath = ADPT::which("dftb+");
    QString ReportFormatter = "Text";
    bool CopyIntermediateFiles = false;
};

struct TestBandStructure
{
    QString name;
    ADPT::UUID uuid;
    QList<BandStructureTestEntry> testRegion;
    double weight = 1.0;
};

class LJDispersionEntry
{
public:
    double distance;
    double energy;
};

class Dispersion
{
public:
    QMap<QString, LJDispersionEntry> m_ljdispersion;
    bool isEmpty() const
    {
        return m_ljdispersion.isEmpty();
    }
};

class Evaluation
{
public:
    Evaluation() = default;
    Evaluation(const Evaluation& rhs);
    std::shared_ptr<ADPT::SKFileInfo> SKInfo;

    QVariantMap dftb_options;
    bool useDampingFactor = false;
    double dampingFactor = 4.0;
    int timeout = 3600;

    DFTBInput mole_default_dftbinput;
    DFTBInput crystal_default_dftbinput;

    EvaluationMolecule evaluationMolecule;
    EvaluationCrystal evaluationCrystal;
    EvaluationReaction evaluationReaction;
};

struct TestingOption
{
    bool punishImaginaryFrequency = false;
    double maxErrorRatio = 0.05;
    ErrorUnit errorUnit;
    QList<TestMolecule> testMolecules;
    QList<TestReaction> testReactions;
    QList<TestBandStructure> testBandStructure;
};

class ReferenceData;
class InputData
{
public:
    Control control;
    Evaluation evaluation;
    TestingOption testingOption;
public:
    bool makeAbsolutePath(const QDir& rootDir);
    QSet<QString> required_rep_pairs(const ReferenceData& ref_data) const;
    QSet<QString> required_elec_pairs(const ReferenceData& ref_data) const;
};

}



#endif // INPUTFILE_H
