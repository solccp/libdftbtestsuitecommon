#-------------------------------------------------
#
# Project created by QtCreator 2013-10-02T22:14:33
#
#-------------------------------------------------

QT       -= gui
TARGET = DFTBTestSuiteCommon
CONFIG += c++11
CONFIG += staticlib
TEMPLATE = lib
QMAKE_CXXFLAGS+=-Wunused-but-set-variable


SOURCES += \
    DFTBTestSuite/ReferenceDataType/moleculegeometry.cpp \
    DFTBTestSuite/ReferenceDataType/MolecularData.cpp \
    DFTBTestSuite/ReferenceDataType/CrystalData.cpp \
    DFTBTestSuite/InputFile.cpp \
    DFTBTestSuite/util.cpp


HEADERS += \
    DFTBTestSuite/ReferenceDataType/ReferenceData.h \
    DFTBTestSuite/ReferenceDataType/ReactionData.h \
    DFTBTestSuite/ReferenceDataType/moleculegeometry.h \
    DFTBTestSuite/ReferenceDataType/MolecularData.h \
    DFTBTestSuite/ReferenceDataType/CrystalData.h \
    DFTBTestSuite/InputFile.h \
    DFTBTestSuite/ReferenceDataType/AtomicData.h \
    DFTBTestSuite/input_variant.h \
    DFTBTestSuite/ReferenceDataType/referencedata_variant.h \
    DFTBTestSuite/util.h

INCLUDEPATH += $$PWD/../external_libs/include/
LIBS += $$PWD/../external_libs/libs/libVariant.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a
